﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xt_net_web_hw
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 1");
            Task_1.TestTask();
            Console.Clear();

            Console.WriteLine("Task 2");
            Task_2.TestTask();
            Console.Clear();

            Console.WriteLine("Task 3");
            Task_3.TestTask();
            Console.Clear();

            Console.WriteLine("Task 4");
            Task_4.Task();
            Console.Clear();

            //Console.ReadKey();
        }
    }
}
